'use strict';

var xAxisTextValue = document.querySelector('#xAxis');
var yAxisTextValue = document.querySelector('#yAxis');
var zAxisTextValue = document.querySelector('#zAxis');
var ball = document.querySelector('#ball');

/*
    --------------
    PositionObject
    --------------
*/
function PositionObject() {
    var _this = this;

    this.direction = {
        left: 0,
        forward: 1,
        right: 0,
        back: 0,
        center: 0
    };

    this.directionElement = {
        left: null,
        forward: null,
        right: null,
        back: null,
        center: null
    };

    this.directionToCompassMap = {
        left: 'W',
        forward: 'N',
        right: 'E',
        back: 'S',
        center: 'C'
    };

    this.directionToCompass = function () {
        for (var pos in _this.direction) {
            if (_this.direction[pos] === 1) {
                void 0;
                return _this.directionToCompassMap[pos];
            }
        }
        void 0;
        void 0;
    };

    this.nullDirection = function () {
        for (var pos in _this.direction) {
            _this.direction[pos] = 0;
        }
        return _this;
    };

    this.toggleDirectionSelector = function (toActivateNewDirection) {
        // Deactivate all elements. It's 5:30 a.m., fuck this shit
        for (var _el in _this.directionElement) {
            if (_this.directionElement[_el].className.includes('active')) {
                _this.directionElement[_el].className = _this.directionElement[_el].className.replace(" active", '');
            }
        }

        var _newEl = _this.directionElement[toActivateNewDirection];
        _newEl.className = _newEl.className + " active";
    };

    this.initSelectors = function () {
        for (var pos in _this.directionElement) {
            _this.directionElement[pos] = document.querySelector('#' + pos);
            void 0;
            void 0;
        }
        return _this;
    };

    // We only decide direction and make the request every x milliseconds
    var _lastChecked = 0;
    this.decideDirection = function (x, y) {

        if (x < -10) {
            void 0;
            _this.moveForward();
        } else if (x > 10) {
            void 0;
            _this.moveBack();
        } else if (y < -10) {
            void 0;
            _this.moveLeft();
        } else if (y > 10) {
            void 0;
            _this.moveRight();
        } else {
            //console.log(`moveCenter x = ${x} | y = ${y}`);
            //this.moveCenter();
        }

        if (_lastChecked === 0) {
            //_lastChecked = Math.round((new Date()).getTime() / 1000);
            _lastChecked = Math.round(new Date().getTime());
            void 0;
        }

        // 400 milliseconds
        if (Math.round(new Date().getTime()) - _lastChecked > 500) {
            void 0;
            _lastChecked = 0;
            _this.makeRequest();
        }

        return _this;
    };

    this.makeRequest = function () {

        fetch('/' + _this.directionToCompass(), {
            method: 'GET'
        }).then(function (Data) {
            //
        }).catch(function (Err) {
            //
        });
    };

    this.setDirection = function (newDirection) {
        _this.nullDirection();
        _this.direction[newDirection] = 1;
        _this.toggleDirectionSelector(newDirection);
    };

    this.moveLeft = function () {
        _this.setDirection('left');
    };

    this.moveForward = function () {
        _this.setDirection('forward');
    };

    this.moveRight = function () {
        _this.setDirection('right');
    };

    this.moveBack = function () {
        _this.setDirection('back');
    };

    this.moveCenter = function () {
        _this.setDirection('center');
    };
}

/*
-----------------------
PositionObject instance
-----------------------
*/
var gyroPosition = new PositionObject();
gyroPosition.initSelectors();

/*
-----------------------------------------------
Device orientation poor's man detection
-----------------------------------------------
 */
if (typeof DeviceOrientationEvent !== 'function') {
    deviceOrientationSupported = false;
    ball.textContent = 'Orientation is not supported on your device. Allow Location Services, including for your browser.';
    document.querySelector('#invalidGyroscope').style.display = 'none';
}

/*
----------------------
Device orientation API
----------------------
 */
window.addEventListener('deviceorientation', DeviceOrientationFn, true);

function DeviceOrientationFn(Ev) {

    var x = Ev.beta; // -180 ... 180. Motion of the device around the X axis
    var y = Ev.gamma; // -90 ... 90. Motion of the event around the Y axis
    var z = Ev.alpha; // 0 ... 360. Motion of the device around the Z axis

    xAxisTextValue.textContent = x.toFixed();
    yAxisTextValue.textContent = y.toFixed();
    zAxisTextValue.textContent = z.toFixed();

    gyroPosition.decideDirection(x.toFixed(), y.toFixed());
}