const app = require('express')();
const http = require('http').Server(app);
//const Io = require('socket.io')(http);

http.listen(3000, () => {
    console.log(`[${(new Date()).toISOString()}][STARTUP] Listening on *:3000`);
});

app.get('/', (Request, Response) => {
    Response.header('Pragma', 'no-cache');
    Response.header('Expires', '-1');
    Response.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    Response.sendFile(__dirname + "/index.html");
});

// Io.on('connection', (Socket) => {
//
//     console.log(`[${(new Date()).toISOString()}][HELLO] User connected.`);
//
//     Socket.on('disconnect', () => {
//         console.log(`[${(new Date()).toISOString()}][BYE] User disconnected!`);
//     });
//
//     Socket.on('newChatMessage', (Msg) => {
//         console.log(`[${(new Date()).toISOString()}]New message: ${Msg}`);
//         Io.emit('newChatMessage', Msg);
//     });
// });