const gulp = require('gulp');
const babel = require('gulp-babel');
const htmlmin = require('gulp-htmlmin');
const minify = require('gulp-minify');
const stripDebug = require('gulp-strip-debug');

gulp.task('default', () => {
    
    gulp.src('index.js')
    .pipe(babel({
        presets: ['env']
    }))
    .pipe(minify({
        ext: {
            src: '.js',
            min: '.min.js'
        }
    }))
    .pipe(stripDebug())
    .pipe(gulp.dest('finished'));
    
    gulp.src('index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('finished'));
});