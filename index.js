let xAxisTextValue = document.querySelector('#xAxis');
let yAxisTextValue = document.querySelector('#yAxis');
let zAxisTextValue = document.querySelector('#zAxis');
let ball = document.querySelector('#ball');

/*
    --------------
    PositionObject
    --------------
*/
function PositionObject() {
    
    this.direction = {
        left: 0,
        forward: 1,
        right: 0,
        back: 0,
        center: 0
    };
    
    this.directionElement = {
        left: null,
        forward: null,
        right: null,
        back: null,
        center: null
    };
    
    this.directionToCompassMap = {
        left: 'W',
        forward: 'N',
        right: 'E',
        back: 'S',
        center: 'C'
    };
    
    this.directionToCompass = () => {
        for (let pos in this.direction) {
            if (this.direction[pos] === 1) {
                console.log(`this.directionToCompassMap[pos] = ${this.directionToCompassMap[pos]}`);
                return this.directionToCompassMap[pos];
            }
        }
        console.log(this.direction);
        console.log(`ERR, am lovit fundul sacului.`);
    };
    
    this.nullDirection = () => {
        for (let pos in this.direction) {
            this.direction[pos] = 0;
        }
        return this;
    };
    
    this.toggleDirectionSelector = (toActivateNewDirection) => {
        // Deactivate all elements. It's 5:30 a.m., fuck this shit
        for (let _el in this.directionElement) {
            if (this.directionElement[_el].className.includes('active')) {
                this.directionElement[_el].className = this.directionElement[_el].className.replace(" active", '');
            }
        }
        
        let _newEl = this.directionElement[toActivateNewDirection];
        _newEl.className = _newEl.className + " active";
    };
    
    this.initSelectors = () => {
        for (let pos in this.directionElement) {
            this.directionElement[pos] = document.querySelector(`#${pos}`);
            console.log(`this.directionElement[pos] = ${this.directionElement[pos]}`);
            console.log(`document.querySelector(#${pos})`);
        }
        return this;
    };
    
    // We only decide direction and make the request every x milliseconds
    let _lastChecked = 0;
    this.decideDirection = (x, y) => {
        
        if (x < -10) {
            console.log(`moveForward x = ${x} | y = ${y}`);
            this.moveForward();
        } else if (x > 10) {
            console.log(`moveBack x = ${x} | y = ${y}`);
            this.moveBack();
        } else if (y < -10) {
            console.log(`moveLeft x = ${x} | y = ${y}`);
            this.moveLeft();
        } else if (y > 10) {
            console.log(`moveRight x = ${x} | y = ${y}`);
            this.moveRight();
        } else {
            //console.log(`moveCenter x = ${x} | y = ${y}`);
            //this.moveCenter();
        }
        
        if (_lastChecked === 0) {
            //_lastChecked = Math.round((new Date()).getTime() / 1000);
            _lastChecked = Math.round((new Date()).getTime());
            console.log("Updated _lastchecked to " + _lastChecked);
        }
        
        // 400 milliseconds
        if ((Math.round((new Date()).getTime()) - _lastChecked) > 500) {
            console.log(`${Math.round((new Date()).getTime()) - _lastChecked} ... More than 500 ms passed. _lastchecked = 0 && made a GET request`);
            _lastChecked = 0;
            this.makeRequest();
        }
        
        return this;
    };
    
    this.makeRequest = () => {
        
        fetch(`/${this.directionToCompass()}`, {
            method: 'GET'
        }).then((Data) => {
            //
        }).catch((Err) => {
            //
        });
    };
    
    this.setDirection = (newDirection) => {
        this.nullDirection();
        this.direction[newDirection] = 1;
        this.toggleDirectionSelector(newDirection);
    };
    
    this.moveLeft = () => {
        this.setDirection('left');
    };
    
    this.moveForward = () => {
        this.setDirection('forward');
    };
    
    this.moveRight = () => {
        this.setDirection('right');
    };
    
    this.moveBack = () => {
        this.setDirection('back');
    };
    
    this.moveCenter = () => {
        this.setDirection('center');
    };
}

/*
-----------------------
PositionObject instance
-----------------------
*/
let gyroPosition = new PositionObject();
gyroPosition.initSelectors();

/*
-----------------------------------------------
Device orientation poor's man detection
-----------------------------------------------
 */
if (typeof DeviceOrientationEvent !== 'function') {
    deviceOrientationSupported = false;
    ball.textContent = `Orientation is not supported on your device. Allow Location Services, including for your browser.`;
    document.querySelector('#invalidGyroscope').style.display = 'none';
}

/*
----------------------
Device orientation API
----------------------
 */
window.addEventListener('deviceorientation', DeviceOrientationFn, true);

function DeviceOrientationFn(Ev) {
    
    let x = Ev.beta; // -180 ... 180. Motion of the device around the X axis
    let y = Ev.gamma; // -90 ... 90. Motion of the event around the Y axis
    let z = Ev.alpha; // 0 ... 360. Motion of the device around the Z axis
    
    xAxisTextValue.textContent = x.toFixed();
    yAxisTextValue.textContent = y.toFixed();
    zAxisTextValue.textContent = z.toFixed();
    
    gyroPosition.decideDirection(x.toFixed(), y.toFixed());
}
